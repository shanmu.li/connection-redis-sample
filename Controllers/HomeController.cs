﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Redis_Test.Models;
using StackExchange.Redis;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Redis_Test.Controllers
{


    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private static IDistributedCache _distributedCache;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {

            #region connection Redis

            ConnectionMultiplexer connection = ConnectionMultiplexer.Connect(
                "hostname:6379,password=xxxxx");

            var db = connection.GetDatabase();

            if (!db.KeyExists("cachekey"))
            {
                ViewBag.IsLoadFromCache = false;
                db.StringSet("cachekey", ObjectToByteArrayStr(new UserModel()
                {
                    Id = 1,
                    Name = "John"
                }), new TimeSpan(0, 1, 0)); //Set Expire Time
            }
            else
            {
                ViewBag.IsLoadFromCache = true;
                var model = StrByteArrayToObject<UserModel>(db.StringGet("cachekey"));
                ViewBag.CacheValue = model.Name;
            }
            connection.Close();

            #endregion

            

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private string ObjectToByteArrayStr(object obj)
        {
            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, obj);
                return System.Text.Encoding.Default.GetString(memoryStream.ToArray());
            }
        }

        private T StrByteArrayToObject<T>(string str)
        {
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(str);
            using (var memoryStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter();
                memoryStream.Write(bytes, 0, bytes.Length);
                memoryStream.Seek(0, SeekOrigin.Begin);
                var obj = binaryFormatter.Deserialize(memoryStream);
                return (T)obj;
            }
        }


    }
}
