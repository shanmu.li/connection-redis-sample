﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redis_Test.Models
{
    [Serializable]
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
